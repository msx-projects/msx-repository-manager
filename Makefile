BASE_PATH=../msx-devel-toolchain-for-macos
SDCC=${BASE_PATH}/tools/sdcc/bin
CC=$(SDCC)/sdcc
ASM=$(SDCC)/sdasz80
HEX2BIN=${BASE_PATH}/tools/hex2bin/hex2bin
INC=${BASE_PATH}/libraries
EXEC=msxget

# One notice the --no-std-crt0 parameter, that indicates the compiler that should not use the default startup code for Z80, but we provide
# by command line. XXXX indicates code location: for crt0msx_msxdos.s XXXX = 0x0107 and for crt0msx_msxdos_advanced.s XXXX = 0x0178. Global variables will be allocated just after code.
#
# Advanced = int main(char** argv, int argc)
# Normal = void main()
#
# http://msx.atlantes.org/index_en.html
LOC=0x0178

CFLAGS=-mz80 --code-loc $(LOC) --data-loc 0 --no-std-crt0 --disable-warning 196  --disable-warning 85 -I$(INC)/base64lib -I$(INC)/dos -I$(INC)/types -I$(INC)/asmlib  $(INC)/asmlib/asm.lib

all: $(EXEC)
 
$(EXEC): clean $(EXEC).ihx
	$(HEX2BIN) $(EXEC).ihx
	mv $(EXEC).bin $(EXEC).com
		
$(EXEC).ihx: crt0msx_msxdos_advanced.rel putchar.rel getchar.rel base64.rel
	$(CC) $(CFLAGS) crt0msx_msxdos_advanced.rel putchar.rel getchar.rel base64.rel $(EXEC).c
        
crt0msx_msxdos_advanced.rel:
	$(ASM) -o crt0msx_msxdos_advanced.rel $(INC)/crt0_msxdos/crt0msx_msxdos_advanced.asm

base64.rel:
	$(ASM) -o base64.rel $(INC)/base64lib/base64.asm

putchar.rel:
	$(ASM) -o putchar.rel $(INC)/putchar/putchar.asm

getchar.rel:
	$(ASM) -o getchar.rel $(INC)/getchar/getchar.asm

clean:
	rm -rf *.asm *.rel *.ihx *.lk *.lst *.map *.noi *.sym *~ $(EXEC).com
